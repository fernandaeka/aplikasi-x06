package novitasari.fernanda.app6

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_mata_kuliah.*
import kotlinx.android.synthetic.main.frag_data_mhs.view.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*


class FragmentNilai : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaMahasiswa = c.getString(c.getColumnIndex("_id"))
        namaMatkul = c.getString(c.getColumnIndex("_id"))

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteNilai ->{

            }
            R.id.btnUpdateNilai ->{

            }
            R.id.btnInsertNilai ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaMahasiswa : String = ""
    var namaMatkul : String = ""
    lateinit var db : SQLiteDatabase
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_mhs,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteNilai.setOnClickListener(this)
        v.btnInsertNilai.setOnClickListener(this)
        v.btnUpdateNilai.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this


        return v
    }

    override fun onStart() {
        super.onStart()
        showDataNilai()

    }


    fun showDataNilai(){
        val c : Cursor = db.rawQuery("select nama_nilai as _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataNilai(namaMhs : String, namaMatkul: String, id_nilai : Int){
        var sql = "insert into mhs(nim, nama, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(namaMhs,namaMatkul,id_nilai))
        showDataNilai()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_matkul from Matkul where nama_nilai='$namaMatkul'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
                insertDataNilai(v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edNilai.setText("")
        }
    }




}